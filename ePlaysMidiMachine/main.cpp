#include "mainwindow.h"
#include "src/papplication.h"

int main(int argc, char *argv[])
{
    PApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}
