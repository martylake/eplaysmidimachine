#ifndef PAPPLICATION_H
#define PAPPLICATION_H

#include <QApplication>

class PApplication : public QApplication
{
    Q_OBJECT
public:
    explicit PApplication(int &argc, char **argv);

signals:

public slots:

};

#endif // PAPPLICATION_H
