#include "klmidikernel.h"

#include <QAction>

#include "RtMidi/RtMidi.h"

KLMidiKernel *KLMidiKernel::self = nullptr;

KLMidiKernel::KLMidiKernel(QObject *parent) :
    QObject(parent), midiOut(nullptr), portOpened(false)
{
    if (self) qFatal("KLMidiKernel : There should be only one MIDI Kernel instance");
    self = this;

    midiOut = new RtMidiOut(RtMidi::UNSPECIFIED, "midiOut");
}

KLMidiKernel::~KLMidiKernel()
{
    delete midiOut;
    self = nullptr;
}

int KLMidiKernel::getMidiOutPortCount() const
{
    return midiOut->getPortCount();
}

const QString KLMidiKernel::getMidiOutPortName(uint portNumber) const
{
    return midiOut->getPortName(portNumber).c_str();
}

void KLMidiKernel::sendMessage(uchar statusByte, uchar dataByte1, uchar dataByte2) const
{
    if (midiOut && portOpened) {
        std::vector<uchar> message(3);
        message.at(0) = statusByte;
        message.at(1) = dataByte1;
        message.at(2) = dataByte2;
        midiOut->sendMessage(&message);
    }
}

void KLMidiKernel::panic() const
{
    if (midiOut && portOpened) {
        std::vector<uchar> panicMessage(3);
        panicMessage.at(0) = 0xB0;
        panicMessage.at(1) = 123;
        panicMessage.at(2) = 0;
        for (uchar iChannel = 0; iChannel < 16; iChannel++) {
            midiOut->sendMessage(&panicMessage);
            panicMessage.at(0) += 1;
        }
    }
}

void KLMidiKernel::openPort(QAction *portAction)
{
    if (midiOut) {
        midiOut->closePort();
        portOpened = false;
        uint portCount = midiOut->getPortCount();
        for (uint iPort = 0; iPort < portCount; iPort++) {
            if (midiOut->getPortName(iPort).c_str() == portAction->text()) {
                try {
                    midiOut->openPort(iPort);
                    portOpened = true;
                    break;
                } catch (const RtError &error) {
                    return;
                }
            }
        }
    }
}
