#pragma once

#include <QObject>

class QAction;

class RtMidiOut;

class KLMidiKernel : public QObject
{
    Q_OBJECT

public:
    explicit KLMidiKernel(QObject *parent = nullptr);
    KLMidiKernel(const KLMidiKernel &) = delete;
    KLMidiKernel &operator =(const KLMidiKernel &) = delete;
    virtual ~KLMidiKernel();

    static KLMidiKernel *instance() { return self; }

    int getMidiOutPortCount() const;
    const QString getMidiOutPortName(uint portNumber) const;

    void sendMessage(uchar statusByte, uchar dataByte1, uchar dataByte2 = 0) const;
    void panic() const;

public slots:
    void openPort(QAction *portAction);

private:
    static KLMidiKernel *self;

    RtMidiOut *midiOut;
    bool portOpened;

};
