#include "papplication.h"
#include "src/RtMidi/RtMidi.h"
#include "src/RtMidi/RtError.h"
#include "src/klmidikernel.h"

PApplication::PApplication(int &argc, char **argv) :
    QApplication(argc, argv)
{
    setApplicationName("Midi Machine");
    new KLMidiKernel(this);
}
