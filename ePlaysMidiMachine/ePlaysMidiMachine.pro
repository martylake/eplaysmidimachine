#-------------------------------------------------
#
# Project created by QtCreator 2013-07-15T20:00:08
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ePlaysMidiMachine
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    src/RtMidi/RtMidi.cpp \
    src/papplication.cpp \
    src/klmidikernel.cpp

HEADERS  += mainwindow.h \
    src/RtMidi/RtMidi.h \
    src/RtMidi/RtError.h \
    src/papplication.h \
    src/klmidikernel.h

FORMS    += mainwindow.ui
